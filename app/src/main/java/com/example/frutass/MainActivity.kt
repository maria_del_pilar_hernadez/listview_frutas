package com.example.frutass

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Limon")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Uvas")
        frutas.add("kiwis")

        val lista = findViewById<ListView>(R.id.listaF)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        listaF.adapter= adaptador

        listaF.setOnItemClickListener { parent: AdapterView<*>, view: View, position: Int, id: Long ->

            if (position == 0) {
            //Se prepara el llamado a la segunda pantalla
            val intent = Intent(this, limon::class.java)

            //No se que mas agregar para mandar a la otra activity con el listview
            startActivity(intent)

        }

            if (position == 1) {
            //Se prepara el llamado a la segunda pantalla
            val intent = Intent(this, manzana::class.java)

            //No se que mas agregar para mandar a la otra activity con el listview
            startActivity(intent)

        }
            if (position == 2) {
                //Se prepara el llamado a la segunda pantalla
                val intent = Intent(this, naranja::class.java)

                //No se que mas agregar para mandar a la otra activity con el listview
                startActivity(intent)

            }
            if (position == 3) {
            //Se prepara el llamado a la segunda pantalla
            val intent = Intent(this, uvas::class.java)

            //No se que mas agregar para mandar a la otra activity con el listview
            startActivity(intent)

        }
            if (position == 4) {
                //Se prepara el llamado a la segunda pantalla
                val intent = Intent(this, kiwi::class.java)

                //No se que mas agregar para mandar a la otra activity con el listview
                startActivity(intent)

            }





        }


        }

    }
